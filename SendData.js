import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet } from 'react-native'

import * as ExpoLocation from 'expo-location'

export default class SendData extends React.Component {
  state = {
    location: { coords: { speed: -1 } },
    errorMessage: ''
  }

  location = ''

  // Duderstadt Center

  _getLocation = async () => {
    const { status } = await ExpoLocation.requestForegroundPermissionsAsync()
    if (status !== 'granted') {
      console.log('Permission to access location was denied')

      this.setState({
        errorMessage: 'Permission Not Granted'
      })
    }

    // console.log('Perimissions Granted')
    const location = await ExpoLocation.getCurrentPositionAsync({
      accuracy: ExpoLocation.Accuracy.BestForNavigation
    })
    // console.log(location)
    this.setState({ location: location })
    // console.log(this.state)
  }

  async componentDidUpdate () {
    await this._getLocation()
    console.log('Location Retrieved')

    let serverData = this.props.serverData

    speed = Math.round(this.state.location.coords.speed * 2.237)
    if (speed < 0) {
      speed = 0
    }

    serverData['speed'] = speed // conversion from m/s to mph
    // if (serverData['direction']) {
    //   serverData['speed'] = serverData['speed'] + 0.5
    // }
    serverData['testing'] = true

    // console.log (serverData)

    try {
      await fetch('https://starlit-sable-3d5cbe.netlify.app/api/main', {
        method: 'post',
        mode: 'no-cors',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(serverData)
      })
      console.log(serverData)
    } catch (e) {
      console.log(e)
    }
  }

  render () {
    return <View></View>
  }
}
