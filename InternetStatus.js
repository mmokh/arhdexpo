import React, { Component } from 'react'
import { useFonts, Roboto } from '@expo-google-fonts/roboto'
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native'
import NetInfo, {NetInfoSubscription, NetInfoState} from '@react-native-community/netinfo'

import notice from './assets/notice.png'


export default class IsConnected extends Component {

  state = {
    isConnected: false,
  };

  componentDidMount() {
    this._getsubbed()
  }

  componentWillUnmount() {
    // Unsubscribes from updates
    this._subscription && this._subscription();
  }

  _getsubbed = async() => {
    this._subscription = NetInfo.addEventListener(s => {
        console.log('Is connected?', s.isConnected);
        this.setState({
            isConnected: s.isConnected
        })
    });
  }

    render () {
    // var connected = false;
    // NetInfo.fetch().then(state => {
    //     console.log('Is connected?', state.isConnected);
    //     connected = state.isConnected
    //     if (state.isConnected) {
    //         return (
    //             <TouchableOpacity disabled={true} style={styles.connection}>
    //             <Image source={notice} style={styles.notice} />
    //             <Text style={styles.connectionText}>
    //             Connected :3
    //             </Text>
    //             </TouchableOpacity>
    //         )   
    //     }
    // })
    // console.log("Is this working 1: ", this.state.isConnected)

    if (this.state.isConnected) {
        return (
            <TouchableOpacity disabled={true} style={styles.yes_connection}>
            <Text style={styles.yes_connectionText}>
            Your HoloLens is connected.
            </Text>
            </TouchableOpacity>
        )   
    }

    // console.log("Is this working 2: ", this.state.isConnected)
    
    return (
        <TouchableOpacity disabled={true} style={styles.no_connection}>
        <Image source={notice} style={styles.notice} />
        <Text style={styles.connectionText}>
          Please make sure your phone and HoloLens are connected to a wifi
          network.
        </Text>
        </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
    yes_connection: {
      top: 230,
      alignSelf: 'center',
      height: 100,
      width: 355,
      backgroundColor: '#62BD59',
      borderRadius: 12,
      borderColor: '#25523B',
      borderWidth: 3
    },
    yes_connectionText: {
      fontSize: 20,
      color: '#FFFFFF',
      fontFamily: Roboto,
      fontWeight: '500',
      marginLeft: 40,
      marginRight: 5,
      top: 7,
      position: 'absolute'
    },
    no_connection: {
      top: 230,
      alignSelf: 'center',
      height: 100,
      width: 355,
      backgroundColor: '#FFF0F1',
      borderRadius: 12,
      borderColor: '#FD0000',
      borderWidth: 3
    },
    notice: {
      height: 40,
      width: 40,
      top: 30,
      marginLeft: 10
    },
    connectionText: {
      fontSize: 16,
      color: '#F54B4B',
      fontFamily: Roboto,
      fontWeight: '500',
      marginLeft: 60,
      marginRight: 5,
      top: 7,
      position: 'absolute'
    }  
})
  