import React, { Component } from 'react'
import { View, Text } from 'react-native'
import SendData from './SendData'

export default class TimeComponent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      seconds: 0
    }
  }

  tick () {
    console.log ("TICK")
    this.setState(state => ({
      seconds: state.seconds + 1
    }))
  }

  componentDidMount () {
    this.interval = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount () {
    clearInterval(this.interval)
  }

  render () {
    // console.log(this.state.seconds)
    return (
      <View>
        {/* <Text>Time Test: {this.state.seconds}</Text> */}
        <SendData serverData={this.props.serverData} />
      </View>
    )
  }
}
