import 'react-native-gesture-handler'
import { StatusBar } from 'expo-status-bar'
import React, { useState, useEffect } from 'react'
import {
  Button,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native'
//import { useFonts, Roboto } from '@expo-google-fonts/roboto'
import { useFonts } from 'expo-font'
import { NavigationContainer, useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import hololens from './assets/hololens.png'
import settings from './assets/settings.png'
import notice from './assets/notice.png'
import back_arrow from './assets/back_arrow.png'
import customize from './assets/customize.png'
import navigation_icon from './assets/navigation.png'
import selected from './assets/selected.png'
import settings_arrow from './assets/settings_arrow.png'
import font_size from './assets/font_size.png'
import fonts_logo from './assets/fonts_logo.png'
import font_selected from './assets/font_selected.png'
import settings_line from './assets/settings_line.png'
import { Marker } from 'react-native-maps'
import MapView from 'react-native-maps'
import MapViewDirections from 'react-native-maps-directions'
import RenderHtml from 'react-native-render-html'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'

import InternetStatus from './InternetStatus'
import TimeComponent from './TimeComponent'

import * as Location from 'expo-location'

const GOOGLE_MAPS_APIKEY = 'AIzaSyBDc4cqIWo5gSVhzynFqNKNVbsoJjtklxk'

var serverData = {
  speed: 10,
  speedometerActive: false,
  navigationActive: false
}

function cacheFonts (font) {
  return fonts.map(font => Font.loadAsync(font))
}

const Customization = () => {
  const navigation = useNavigation()
  const [isSpeedometerActive, setIsSpeedometerActive] = useState(false)
  const [isGPSNavActive, setIsGPSNavActive] = useState(false)
  const [isGPSLaneActive, setIsGPSLaneActive] = useState(false)
  const [isMusicActive, setIsMusicActive] = useState(false)
  const handleSpeedometerClick = () => {
    setIsSpeedometerActive(current => !current)
    serverData['speedometerActive'] = !serverData['speedometerActive']
  }
  const handleGPSNavClick = () => {
    setIsGPSNavActive(current => !current)
    serverData['navigationActive'] = !serverData['navigationActive']
  }
  const handleGPSLaneClick = () => {
    setIsGPSLaneActive(current => !current)
  }
  const handleMusicClick = () => {
    setIsMusicActive(current => !current)
  }
  return (
    <View style={styles.inner_container}>
      <View style={styles.hololens_header}>
        <TimeComponent serverData={serverData} />
        <Text style={styles.header_text}>Customize Features</Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('Home')}
          accessible={true}
          accessibilityLabel='Navigation'
          accessibilityHint='Go to Navigation page'
          style={styles.back_button}
        >
          <Image source={back_arrow} style={styles.back_button_image}></Image>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        onPress={handleSpeedometerClick}
        style={[
          styles.select_display_feature,
          { backgroundColor: isSpeedometerActive ? '#00D73C' : '#FF650F' }
        ]}
      >
        <View
          style={[
            styles.inner_unselected_box,
            { backgroundColor: isSpeedometerActive ? '#D0FFAF' : '#FFBFB7' }
          ]}
        >
          {isSpeedometerActive && (
            <Image source={selected} style={styles.check}></Image>
          )}
        </View>
        <Text style={styles.selection_button_text}>Speedometer</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={handleGPSNavClick}
        style={[
          styles.select_display_feature,
          { backgroundColor: isGPSNavActive ? '#00D73C' : '#FF650F' }
        ]}
      >
        <View
          style={[
            styles.inner_unselected_box,
            { backgroundColor: isGPSNavActive ? '#D0FFAF' : '#FFBFB7' }
          ]}
        >
          {isGPSNavActive && (
            <Image source={selected} style={styles.check}></Image>
          )}
        </View>
        <Text style={styles.selection_button_text}>GPS - Navigation</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={handleGPSLaneClick}
        style={[
          styles.select_display_feature,
          { backgroundColor: isGPSLaneActive ? '#00D73C' : '#FF650F' }
        ]}
      >
        <View
          style={[
            styles.inner_unselected_box,
            { backgroundColor: isGPSLaneActive ? '#D0FFAF' : '#FFBFB7' }
          ]}
        >
          {isGPSLaneActive && (
            <Image source={selected} style={styles.check}></Image>
          )}
        </View>
        <Text style={styles.selection_button_text}>GPS - Lanes</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={handleMusicClick}
        style={[
          styles.select_display_feature,
          { backgroundColor: isMusicActive ? '#00D73C' : '#FF650F' }
        ]}
      >
        <View
          style={[
            styles.inner_unselected_box,
            { backgroundColor: isMusicActive ? '#D0FFAF' : '#FFBFB7' }
          ]}
        >
          {isMusicActive && (
            <Image source={selected} style={styles.check}></Image>
          )}
        </View>
        <Text style={styles.selection_button_text}>Music Playback</Text>
      </TouchableOpacity>
      <View style={styles.bottom_tab_navigator}>
        <TouchableOpacity disabled={true} style={styles.customize_page_button}>
          <Image source={customize} style={styles.tab_image} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('GPS')}
          accessible={true}
          accessibilityLabel='Customization'
          accessibilityHint='Go to Customization page'
          style={styles.navigation_page_button}
        >
          <Image source={navigation_icon} style={styles.tab_image} />
        </TouchableOpacity>
      </View>
    </View>
  )
}

const GPS = () => {
  const navigation = useNavigation()

  let mapRef
  let navInterval

  const [autocompleteDest, setAutocompleteDest] = useState('')

  const [destination, setDestination] = useState({})

  const [directions, setDirections] = useState([])

  const [currentPage, setCurrentPage] = useState(0)

  const [location, setLocation] = useState({})
  const [errorMsg, setErrorMsg] = useState(null)

  const [startAddress, setStartAddress] = useState('')

  useEffect(() => {
    ;(async () => {
      console.log('getting user location')
      let { status } = await Location.requestForegroundPermissionsAsync()
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied')
        return
      }

      let location = await Location.getCurrentPositionAsync({})
      console.log('current location: ', location)
      setLocation({
        latitude: location['coords']['latitude'],
        longitude: location['coords']['longitude']
      })
    })()
  }, [])

  const getLocation = async () => {
    let location = await Location.getCurrentPositionAsync({
      accuracy: ExpoLocation.Accuracy.BestForNavigation
    })
    return {
      latitude: location['coords']['latitude'],
      longitude: location['coords']['longitude']
    }
  }

  const getDirections = async () => {
    console.log('DESTINATION', autocompleteDest)
    let addressToCoords = `https://api.mapbox.com/geocoding/v5/mapbox.places/${autocompleteDest}.json?access_token=pk.eyJ1Ijoic3NhcmFidSIsImEiOiJjbGE4cTBhYzQwNmFzM3ZvNDhrd3lzZXFvIn0.KUKIr8HATUezrJDLsJFihQ`
    let res = await fetch(addressToCoords)
    let data = await res.json()
    console.log(data)
    let destLatLong = data['features'][0]['center']
    console.log(destLatLong[1], destLatLong[0])
    setDestination({ latitude: destLatLong[1], longitude: destLatLong[0] })

    setCurrentPage(1)
  }

  const runNavigation = async () => {
    let location = getLocation()
    // let url = `https://maps.googleapis.com/maps/api/directions/json?origin=${location}&destination=${destination}&mode=driver&sensor=true&key=${GOOGLE_MAPS_APIKEY}`
    let url = `https://api.mapbox.com/directions/v5/mapbox/driving/${location['longitude']},${location['latitude']};${destination['longitude']},${destination['latitude']}?alternatives=true&geometries=geojson&language=en&overview=simplified&steps=true&access_token=pk.eyJ1Ijoic3NhcmFidSIsImEiOiJjbGE4cTBhYzQwNmFzM3ZvNDhrd3lzZXFvIn0.KUKIr8HATUezrJDLsJFihQ`
    let res = await fetch(url)
    let data = await res.json()
    let steps = data['routes'][0]['legs'][0]['steps']
    // console.log(steps)
    // for (var i = 0; i < steps.length; i++) {
    var currstep = steps[0]
    let dir = ''
    let dist = 0
    let street = ''
    if (
      currstep['maneuver']['type'] == 'turn' ||
      currstep['maneuver']['type'] == 'end of road'
    ) {
      dir = currstep['maneuver']['modifier']
    } else {
      dir = currstep['maneuver']['type']
    }
    if (i > 0) {
      if (steps[i - 1]['distance'] / 1609 >= 0.1)
        dist = (steps[i - 1]['distance'] / 1609).toFixed(1) + ' mi'
      else {
        dist = Math.round(steps[i - 1]['distance'] * 3.28084) + ' ft'
      }
      street = currstep['name']
      // }
      dir = capitalizeWords(dir)
      console.log(capitalizeWords(dir), dist, street)
      sendDirections(dir, dist, street)

      // test example comment
    }
  }

  const capitalizeWords = myWords => {
    const words = myWords.replace(/(^\w{1})|(\s+\w{1})/g, letter =>
      letter.toUpperCase()
    )
    return words
  }

  const sendDirections = async (dir, dist, street) => {
    serverData['direction'] = dir
    serverData['distance'] = dist
    serverData['street'] = street
  }

  if (currentPage == 0) {
    return (
      <View style={styles.inner_container}>
        <View style={styles.hololens_header}>
          <Text style={styles.header_text}>Navigation</Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('Home')}
            style={styles.back_button}
          >
            <Image source={back_arrow} style={styles.back_button_image}></Image>
          </TouchableOpacity>
        </View>
        <View style={styles.container}>
          {/* <TextInput
          style={styles.input}
          onChangeText={setDestination}
          value={destination}
          placeholder='Enter your destination'
        /> */}
          <GooglePlacesAutocomplete
            placeholder='Search'
            onPress={(data, details = null) => {
              // 'details' is provided when fetchDetails = true
              console.log('DATA AUTOCOMPLETE')
              setAutocompleteDest(data['description'])
            }}
            query={{
              key: 'AIzaSyBuEpqwPAEh2tmH6x7ug9RAqtyK3KGKHlA',
              language: 'en'
            }}
            styles={{
              container: {
                flex: 0.8,
                justifySelf: 'flex-start'
              },
              textInputContainer: {
                flexDirection: 'row'
              },
              textInput: {
                backgroundColor: '#FFFFFF',
                height: 50,
                borderRadius: 5,
                paddingVertical: 5,
                paddingHorizontal: 10,
                fontSize: 15,
                flex: 1
              },
              poweredContainer: {
                justifyContent: 'flex-end',
                alignItems: 'center',
                borderBottomRightRadius: 5,
                borderBottomLeftRadius: 5,
                borderColor: '#c8c7cc',
                borderTopWidth: 0.5
              },
              powered: {},
              listView: {},
              row: {
                backgroundColor: '#FFFFFF',
                padding: 13,
                height: 44,
                flexDirection: 'row'
              },
              separator: {
                height: 0.5,
                backgroundColor: '#c8c7cc'
              },
              description: {},
              loader: {
                flexDirection: 'row',
                justifyContent: 'flex-end',
                height: 20
              }
            }}
          />
          <TouchableOpacity
            onPress={() => getDirections()}
            accessible={true}
            accessibilityLabel='GPS'
            accessibilityHint='Go to GPS page'
            style={styles.navigateButton}
          >
            <Text
              style={{
                color: 'white',
                fontSize: 20,
                fontFamily: global.currFont
              }}
            >
              Next
            </Text>
          </TouchableOpacity>
          {/* <MapView style={styles.map} initialRegion='US' userInteraction>
          <MapViewDirections
            origin={initialAddress}
            destination={destination}
            apikey={GOOGLE_MAPS_APIKEY}
            strokeWidth={5}
            strokeColor='hotpink'
          />
        </MapView> */}
        </View>
        <View style={styles.bottom_tab_navigator}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Customize')}
            accessible={true}
            accessibilityLabel='GPS'
            accessibilityHint='Go to GPS page'
            style={styles.customize_page_button}
          >
            <Image source={customize} style={styles.tab_image} />
          </TouchableOpacity>
          <TouchableOpacity
            disabled={true}
            style={styles.navigation_page_button}
          >
            <Image source={navigation_icon} style={styles.tab_image} />
          </TouchableOpacity>
        </View>
      </View>
    )
  } else {
    return (
      <View style={styles.inner_container}>
        <View style={styles.hololens_header}>
          <Text style={styles.header_text}>Navigation</Text>
          <TouchableOpacity
            onPress={() => {
              clearInterval(navInterval)
              setCurrentPage(0)
            }}
            style={styles.back_button}
          >
            <Image source={back_arrow} style={styles.back_button_image}></Image>
          </TouchableOpacity>
        </View>
        <View style={styles.container}>
          <MapView
            initialRegion={{
              latitude: location['latitude'],
              longitude: location['longitude'],
              latitudeDelta: 0.05,
              longitudeDelta: 0.06
            }}
            ref={ref => {
              mapRef = ref
            }}
            onMapReady={() =>
              mapRef.fitToCoordinates([location, destination], {
                edgePadding: { top: 30, right: 30, bottom: 30, left: 30 },
                animated: true
              })
            }
            style={{ width: '100%', height: '75%' }}
          >
            <Marker coordinate={location} />
            <Marker coordinate={destination} />
            <MapViewDirections
              origin={location}
              destination={destination}
              apikey={GOOGLE_MAPS_APIKEY}
              strokeWidth={3}
              strokeColor='hotpink'
            />
          </MapView>

          <TouchableOpacity
            onPress={() => {
              navInterval = setInterval(runNavigation, 1000)
            }}
            accessible={true}
            accessibilityLabel='GPS'
            accessibilityHint='Go to GPS page'
            style={styles.navigateButton}
          >
            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}>
              Go
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.bottom_tab_navigator}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Customize')}
            accessible={true}
            accessibilityLabel='GPS'
            accessibilityHint='Go to GPS page'
            style={styles.customize_page_button}
          >
            <Image source={customize} style={styles.tab_image} />
          </TouchableOpacity>
          <TouchableOpacity
            disabled={true}
            style={styles.navigation_page_button}
          >
            <Image source={navigation_icon} style={styles.tab_image} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const HomeScreen = () => {
  const navigation = useNavigation()

  return (
    <View style={styles.container}>
      <TimeComponent serverData={serverData} />
      <Text style={styles.title}>ARHD</Text>
      <Image source={hololens} style={styles.hololens} />
      <StatusBar style='auto' />
      <InternetStatus />
      <TouchableOpacity
        onPress={() => navigation.navigate('Customize')}
        style={styles.button}
      >
        <Text style={styles.buttonText}>Continue</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate('Settings')}
        style={styles.settings_button}
        accessible={true}
        accessibilityLabel='Application Settings'
        accessibilityHint='Adjust font and font sizes in the application'
      >
        <Image source={settings} style={styles.settings_image} />
      </TouchableOpacity>
    </View>
  )
}

const Settings = () => {
  const navigation = useNavigation()

  return (
    <View style={styles.inner_container}>
      <View style={styles.hololens_header}>
        <Text style={styles.header_text}>App Settings</Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('Home')}
          style={styles.back_button}
          accessible={true}
          accessibilityLabel='Back'
          accessibilityHint='Go back to home screen'
        >
          <Image source={back_arrow} style={styles.back_button_image}></Image>
        </TouchableOpacity>
      </View>
      <View style={styles.settings_block}>
        <TouchableOpacity
          style={styles.settings_option}
          onPress={() => navigation.navigate('Font')}
        >
          <Image source={fonts_logo} style={styles.settings_logo}></Image>
          <Text style={styles.settings_text}>Font</Text>
          <Image
            source={settings_arrow}
            style={styles.settings_arrow_position}
          ></Image>
        </TouchableOpacity>
        <Image source={settings_line} style={styles.line}></Image>
        <TouchableOpacity style={styles.settings_option}>
          <Image source={font_size} style={styles.settings_logo}></Image>
          <Text style={styles.settings_text}>Font Size</Text>
          <Image
            source={settings_arrow}
            style={styles.settings_arrow_position}
          ></Image>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const Font = () => {
  const navigation = useNavigation()

  const changeToRoboto = () => {
    global.currFont = 'Roboto'
  }

  const changeToOpenDyslexic = () => {
    global.currFont = 'OpenDyslexic'
  }

  return (
    <View style={styles.inner_container}>
      <View style={styles.hololens_header}>
        <Text style={styles.header_text}>Select Font</Text>
        <TouchableOpacity
          onPress={() => navigation.navigate('Settings')}
          style={styles.back_button}
          accessible={true}
          accessibilityLabel='Back'
          accessibilityHint='Go back to home screen'
        >
          <Image source={back_arrow} style={styles.back_button_image}></Image>
        </TouchableOpacity>
      </View>
      <View style={styles.settings_block}>
        <TouchableOpacity
          style={styles.settings_option}
          onPress={changeToRoboto}
        >
          <Text style={[styles.font_options, { fontFamily: 'Roboto' }]}>
            Roboto
          </Text>
        </TouchableOpacity>
        <Image source={settings_line} style={styles.line}></Image>
        <TouchableOpacity
          style={styles.settings_option}
          onPress={changeToOpenDyslexic}
        >
          <Text style={[styles.font_options, { fontFamily: 'OpenDyslexic' }]}>
            Open Dyslexic
          </Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.font_message}>
        Note: In order to change application font, application must be render
        again. Please recompile the code.
      </Text>
    </View>
  )
}

const Stack = createStackNavigator()

export const AppNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name='Home'
      component={HomeScreen}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name='Settings'
      component={Settings}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name='Customize'
      component={Customization}
      options={{ headerShown: false }}
    />
    <Stack.Screen name='GPS' component={GPS} options={{ headerShown: false }} />
    <Stack.Screen
      name='Font'
      component={Font}
      options={{ headerShown: false }}
    />
  </Stack.Navigator>
)

export default function App () {
  const [fontsLoaded] = useFonts({
    OpenDyslexic: require('./assets/fonts/OpenDyslexic/OpenDyslexic-Bold.otf'),
    Roboto: require('./assets/fonts/Roboto/Roboto-Bold.ttf')
  })

  global.currFont = 'Roboto'
  return (
    <NavigationContainer>
      <AppNavigator />
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000'
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  title: {
    color: '#FFFFFF',
    fontSize: 64,
    top: '10%',
    alignSelf: 'center',
    fontFamily: global.currFont,
    fontWeight: 'bold'
  },
  hololens: {
    width: '60%',
    height: '12%',
    top: '15%',
    alignSelf: 'center'
  },
  connection: {
    top: '25%',
    alignSelf: 'center',
    height: '11%',
    width: '85%',
    backgroundColor: '#FFF0F1',
    borderRadius: 12,
    borderColor: '#FD0000',
    borderWidth: 3
  },
  notice: {
    height: '30%',
    width: '10%',
    top: '30%',
    marginLeft: 10
  },
  input: {
    height: 70,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    fontSize: 20,
    backgroundColor: 'white',
    fontFamily: global.currFont,
    // color: 'white',
    borderColor: 'white'
  },
  connectionText: {
    fontSize: '13%',
    color: '#F54B4B',
    fontFamily: global.currFont,
    fontWeight: '500',
    marginLeft: '15%',
    marginRight: '5%',
    top: '8%',
    position: 'absolute'
  },
  button: {
    backgroundColor: '#537BE2',
    padding: 20,
    borderRadius: 12,
    alignSelf: 'center',
    top: '30%',
    height: '10%',
    width: '75%'
  },
  buttonText: {
    fontSize: 32,
    color: '#FFFFFF',
    alignSelf: 'center',
    fontFamily: global.currFont,
    fontWeight: 'bold'
  },
  settings_button: {
    backgroundColor: '#BCBDB1',
    height: '8%',
    width: '17%',
    top: '40%',
    left: '75%',
    borderRadius: 12
  },
  settings_image: {
    height: '75%',
    width: '75%',
    alignSelf: 'center',
    top: '10%'
  },
  inner_container: {
    flex: 1,
    backgroundColor: '#5E5E5E'
  },
  hololens_header: {
    justifyContent: 'top',
    backgroundColor: '#000000',
    height: '10%'
  },
  header_text: {
    top: 50,
    alignSelf: 'center',
    fontSize: 20,
    fontFamily: global.currFont,
    fontWeight: '500',
    color: '#FFFFFF'
  },
  back_button: {
    backgroundColor: '#000000',
    marginLeft: 0,
    height: '49%',
    width: '15%'
  },
  navigateButton: {
    marginTop: 10,
    height: 60,
    width: 200,
    borderRadius: 12,
    backgroundColor: 'green',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    justifySelf: 'center'
  },
  back_button_image: {
    marginLeft: '25%',
    marginTop: '20%'
  },
  bottom_tab_navigator: {
    width: '100%',
    height: '11%',
    backgroundColor: '#000000',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    flex: 1,
    flexDirection: 'row'
  },
  customize_page_button: {
    backgroundColor: '#000000',
    height: '100%',
    flex: 1
  },
  navigation_page_button: {
    backgroundColor: '#000000',
    height: '100%',
    flex: 1
  },
  select_display_feature: {
    height: '10%',
    width: '90%',
    marginTop: '7%',
    alignSelf: 'center',
    borderRadius: 12
  },
  inner_unselected_box: {
    height: '50%',
    width: '12%',
    borderRadius: 12,
    marginTop: '6%',
    marginLeft: '4%'
  },
  selection_button_text: {
    marginTop: '7%',
    marginLeft: '19%',
    fontSize: 25,
    fontFamily: global.currFont,
    fontWeight: '700',
    color: '#FFFFFF',
    position: 'absolute'
  },
  check: {
    alignSelf: 'center'
  },
  settings_block: {
    width: '90%',
    height: 140,
    marginTop: 30,
    alignSelf: 'center',
    backgroundColor: '#000000',
    borderRadius: 7
  },
  settings_option: {
    backgroundColor: '#000000',
    borderRadius: 7,
    flex: 1,
    padding: 1
  },
  settings_logo: {
    marginTop: 20,
    marginLeft: 15
  },
  settings_arrow_position: {
    position: 'absolute',
    marginTop: 20,
    marginLeft: 355
  },
  line: {
    width: '100%'
  },
  settings_text: {
    marginTop: 17,
    marginLeft: 70,
    fontSize: 20,
    fontFamily: global.currFont,
    fontWeight: '500',
    color: '#FFFFFF',
    position: 'absolute'
  },
  tab_image: {
    height: '50%',
    width: '25%',
    alignSelf: 'center',
    top: '20%'
  },
  font_options: {
    marginTop: 17,
    marginLeft: 20,
    fontSize: 20,
    fontWeight: '500',
    color: '#FFFFFF',
    position: 'absolute'
  },
  font_message: {
    fontFamily: global.currFont,
    marginTop: 20,
    width: '90%',
    color: '#FFFFFF',
    marginLeft: '5%',
    fontSize: '20%'
  }
})
